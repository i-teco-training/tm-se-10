package ru.alekseev.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.alekseev.tm.api.*;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.enumerated.RoleType;
import ru.alekseev.tm.repository.ProjectRepository;
import ru.alekseev.tm.repository.TaskRepository;
import ru.alekseev.tm.repository.UserRepository;
import ru.alekseev.tm.service.*;
import ru.alekseev.tm.util.HashUtil;

import java.util.*;

public final class Bootstrap implements ServiceLocator {
    @NotNull private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull private final IUserRepository userRepository = new UserRepository();
    @NotNull private final IUserService userService = new UserService(userRepository);
    @NotNull private final ITerminalService terminalService = new TerminalService();
    @NotNull private final IDomainService domainService =
            new DomainService(projectRepository, taskRepository, userRepository);

    @NotNull private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.alekseev.tm").getSubTypesOf(AbstractCommand.class);

    @Override
    @NotNull
    public final List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @Override
    @NotNull
    public final IProjectService getProjectService() {
        return projectService;
    }

    @Override
    @NotNull
    public final ITaskService getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public final IUserService getUserService() {
        return userService;
    }

    @Override
    @NotNull
    public final ITerminalService getTerminalService() {
        return terminalService;
    }

    @Override
    @NotNull
    public final IDomainService getDomainService() {
        return domainService;
    }

    public final void registry(@NotNull final AbstractCommand command) {
        @NotNull final String commandName = command.getName();
        commands.put(commandName, command);
    }

    public final void start() throws Exception {
        initCommands(classes);
        initUsers();
        System.out.println("*** WELCOME TO TASK MANAGER ***");

        while (true) {
            @NotNull String commandName = terminalService.getFromConsole();
            if (!commands.containsKey(commandName)) {
                commandName = "help";
            }

            if (commands.get(commandName).isSecure() && !this.userService.isAuthorized()) {
                System.out.println("LOGIN INTO PROJECT MANAGER");
                System.out.println();
                continue;
            }

            try {
                commands.get(commandName).execute();
            } catch (@NotNull Exception e) {
                System.out.println("An exception was thrown");
                e.printStackTrace();
            }
            System.out.println();
        }
    }

    public final void initCommands(@NotNull final Set<Class<? extends AbstractCommand>> classes) throws Exception {
        for (@Nullable final Class commandClass : classes) {
            if (commandClass == null) continue;
            if (AbstractCommand.class.isAssignableFrom(commandClass)) {
                @NotNull final AbstractCommand command = (AbstractCommand) commandClass.newInstance();
                command.setServiceLocator(this);
                registry(command);
            }
        }
    }

    public final void initUsers() {
        this.userService.addByLoginPasswordUserRole("user1", HashUtil.getMd5("www"), RoleType.ADMIN);
        this.userService.addByLoginPasswordUserRole("user2", HashUtil.getMd5("www"), RoleType.USER);
    }
}