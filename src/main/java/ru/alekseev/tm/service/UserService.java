package ru.alekseev.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.IUserRepository;
import ru.alekseev.tm.api.IUserService;
import ru.alekseev.tm.enumerated.RoleType;
import ru.alekseev.tm.entity.User;

import java.util.List;

@Getter
@Setter
public class UserService extends AbstractService<User> implements IUserService {
    private final IUserRepository userRepository;
    public UserService(@NotNull IUserRepository userRepository) {
        this.userRepository = userRepository;
    }
    @Nullable private User currentUser;

    @Override
    public final void delete(@NotNull final String id) {
        userRepository.delete(id);
    }

    @Override
    public final boolean isAuthorized() {
        return this.currentUser != null;
    }

    @Override
    public final void add(@NotNull final User user) {
        userRepository.persist(user);
    }

    @Override
    @NotNull
    public final List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @Nullable
    public final User findOneByLoginAndPassword(@NotNull final String login, @NotNull final String passwordHashcode) {
        if (login.isEmpty() || passwordHashcode.isEmpty()) return null; //ok?
        return userRepository.findOneByLoginAndPasswordHashcode(login,passwordHashcode);
    }

    @Override
    public final void update(@NotNull final User user) {
        userRepository.merge(user);
    }

    @Override
    public final void deleteByLoginAndPassword(@NotNull final String login, @NotNull final String passwordHashcode) {
        if (login.isEmpty() || passwordHashcode.isEmpty()) return;
        userRepository.deleteByLoginAndPassword(login, passwordHashcode);
    }

    public final void clear() {
        userRepository.clear();
    }

    @Override
    public void addByLoginPasswordUserRole(
            @NotNull final String login,
            @NotNull final String passwordHashcode,
            @NotNull final RoleType roleType
    ) {
        if (login.isEmpty() || passwordHashcode.isEmpty() || roleType.toString().isEmpty()) return;
        userRepository.addByLoginPasswordUserRole(login, passwordHashcode, roleType);
    }
}
