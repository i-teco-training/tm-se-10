package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.IProjectRepository;
import ru.alekseev.tm.api.IProjectService;
import ru.alekseev.tm.entity.Project;

import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public final void update(@NotNull final Project project) {
        projectRepository.merge(project);
    }

    @Override
    public final void delete(@NotNull final String id) {
        projectRepository.delete(id);
    }

    @Override
    public final void clear() {
        projectRepository.clear();
    }

    @Override
    public final void add(@NotNull final Project project) {
        projectRepository.persist(project);
    }

    @Override
    @Nullable
    public final List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @Nullable
    public final Project findOneByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return projectRepository.findOneByUserIdAndProjectId(userId, projectId);
    }

    @Nullable
    public final List<Project> findAllByUserId(@NotNull final String userId) {
        if (userId.isEmpty()) return null;
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public final void updateByUserIdProjectIdProjectName(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String projectName
    ) {
        if (userId.isEmpty() || projectId.isEmpty() || projectName.isEmpty()) return;
        projectRepository.updateByUserIdProjectIdProjectName(userId, projectId, projectName);
    }

    @Override
    public final void deleteByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty() || projectId.isEmpty()) return;
        @Nullable final Project projectForExistenceChecking = projectRepository.findOne(projectId);
        if (projectForExistenceChecking.getUserId() == null || projectForExistenceChecking.getUserId().isEmpty())
            return;
        if (!userId.equals(projectForExistenceChecking.getUserId())) return;
        projectRepository.delete(projectId);
    }

    @Override
    public final void clearByUserId(@NotNull final String userId) {
        if (userId.isEmpty()) return;
        projectRepository.deleteByUserId(userId);
    }
}
