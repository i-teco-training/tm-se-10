package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.ITaskRepository;
import ru.alekseev.tm.api.ITaskService;
import ru.alekseev.tm.entity.Task;

import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    @Nullable
    public final List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public final void update(@NotNull final Task task) {
        taskRepository.merge(task);
    }

    @Override
    public final void delete(@NotNull final String id) {
        if (id.isEmpty()) return;
        taskRepository.delete(id);
    }

    @Override
    public final void clear() {
        taskRepository.clear();
    }

    @Override
    public final void add(@NotNull final Task task) {
        taskRepository.persist(task);
    }

    @Override
    @Nullable
    public final List<Task> findAllByUserId(@NotNull final String userId) {
        if (userId.isEmpty()) return null;
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public final void updateByNewData(
            @NotNull final String userId,
            @NotNull final String taskId,
            @NotNull final String name
    ) {
        if (userId.isEmpty() || taskId.isEmpty() || name.isEmpty()) return;
        taskRepository.updateByNewData(userId, taskId, name);
    }

    @Override
    public final void deleteByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) {
        if (userId.isEmpty() || taskId.isEmpty()) return;
        taskRepository.deleteByUserIdAndTaskId(userId, taskId);
    }

    @Override
    public final void clearByProjectId(@NotNull final String projectId) {
        if (projectId.isEmpty()) return;
        taskRepository.clearByProjectId(projectId);
    }
}
