package ru.alekseev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.IProjectRepository;
import ru.alekseev.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    @Nullable
    public final Project findOneByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @Nullable final Project projectForExistenceChecking = map.get(projectId);
        if (projectForExistenceChecking.getUserId() == null || projectForExistenceChecking.getUserId().isEmpty())
            return null;
        if (!userId.equals(projectForExistenceChecking.getUserId())) return null;
        return map.get(projectId);
    }

    @Override
    @NotNull
    public final List<Project> findAllByUserId(@NotNull final String userId) {
        @NotNull final List<Project> allProjects = new ArrayList<>(map.values());
        @NotNull final List<Project> filteredList = new ArrayList<>();
        for (@Nullable final Project project : allProjects) {
            if (project == null) continue;
            if (project.getUserId() == null || project.getUserId().isEmpty()) continue;
            if (project.getUserId().equals(userId))
                filteredList.add(project);
        }
        return filteredList;
    }

    @Override
    public final void deleteByUserId(@NotNull final String userId) {
        @Nullable final List<Project> projects = new ArrayList<>(map.values());
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            if (project.getUserId() == null || project.getUserId().isEmpty()) continue;
            if (project.getUserId().equals(userId))
                map.remove(project);
        }
    }

    @Override
    public final void updateByUserIdProjectIdProjectName(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String projectName
    ) {
        @Nullable Project requiredProject = map.get(projectId);
        if (requiredProject.getUserId().isEmpty() || requiredProject.getUserId() == null) return;
        @Nullable String requiredUserId = requiredProject.getUserId();
        if (userId.equals(requiredUserId)) {
            @NotNull final Project project = new Project();
            project.setUserId(userId);
            project.setId(projectId);
            project.setName(projectName);
            map.put(project.getId(), project);
        }
    }

    @Override
    public final void deleteByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @Nullable final Project projectForExistenceChecking = map.get(projectId);
        if (projectForExistenceChecking.getUserId() == null || projectForExistenceChecking.getUserId().isEmpty())
            return;
        if (!userId.equals(projectForExistenceChecking.getUserId())) return;
        map.remove(projectId);
    }
}
