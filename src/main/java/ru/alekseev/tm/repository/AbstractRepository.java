package ru.alekseev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.IRepository;
import ru.alekseev.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final Map<String, E> map = new HashMap<>();

    @Override
    public void addAll(@Nullable final List<E> list) {
        if (list == null) return;
        for (E entity : list)
            map.put(entity.getId(), entity);
    }

    @Override
    @NotNull
    public final List<E> findAll() {
       return new ArrayList<>(map.values());
    }

    @Override
    @Nullable
    public final E findOne(@NotNull final String id) {
        return map.get(id);
    }

    @Override
    public final void persist(@NotNull final E e) {
        if (!map.containsKey(e.getId())) {
            map.put(e.getId(), e);
        }
    }

    @Override
    public final void merge(@NotNull final E e) {
        map.put(e.getId(), e);
    }

    @Override
    public final void delete(@NotNull final String id) {
        map.remove(id);
    }

    @Override
    public final void clear() {
        map.clear();
    }
}
