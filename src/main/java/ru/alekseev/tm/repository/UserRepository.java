package ru.alekseev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.IUserRepository;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.enumerated.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @Nullable
    public final User findOneByLoginAndPasswordHashcode(
            @NotNull final String login,
            @NotNull final String passwordHashcode
    ) {
        @NotNull final List<User> list = new ArrayList<>(map.values());
        for (@Nullable final User user : list) {
            if (user == null) continue;
            if (login.equals(user.getLogin()) && passwordHashcode.equals(user.getPasswordHashcode()))
                return user;
        }
        return null;
    }

    @Override
    public final void deleteByLoginAndPassword(
            @NotNull final String login,
            @NotNull final String passwordHashcode
    ) {
        @Nullable final User userForExistenceChecking = findOneByLoginAndPasswordHashcode(login, passwordHashcode);
        if (userForExistenceChecking == null) return;
        map.remove(userForExistenceChecking.getId());
    }

    @Override
    public void addByLoginPasswordUserRole(
            @NotNull final String login,
            @NotNull final String passwordHashcode,
            @NotNull final RoleType roleType
    ) {
        @NotNull final User newUser = new User();
        newUser.setLogin(login);
        newUser.setPasswordHashcode(passwordHashcode);
        newUser.setRoleType(roleType);
        map.put(newUser.getId(), newUser);
    }
}
