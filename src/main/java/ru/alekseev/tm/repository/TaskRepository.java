package ru.alekseev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.ITaskRepository;
import ru.alekseev.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    @NotNull
    public final List<Task> findAllByUserId(@NotNull final String userId) {
        @NotNull final List<Task> allTasks = new ArrayList<>(map.values());
        @NotNull final List<Task> filteredList = new ArrayList<>();
        for (@Nullable final Task task : allTasks) {
            if (task == null) continue;
            if (task.getUserId() == null || task.getUserId().isEmpty()) continue;
            if (task.getUserId().equals(userId))
                filteredList.add(task);
        }
        return filteredList;
    }

    @Override
    public final void deleteByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) {
        @Nullable final Task taskForExistenceChecking = map.get(taskId);
        if (taskForExistenceChecking.getUserId() == null | taskForExistenceChecking.getUserId().isEmpty()) return;
        if (!userId.equals(taskForExistenceChecking.getUserId())) return;
        map.remove(taskId);
    }

    @Override
    public final void updateByNewData(
            @NotNull final String userId,
            @NotNull final String taskId,
            @NotNull final String name
    ) {
        @Nullable final Task oldTask = map.get(taskId);
        if (oldTask.getProjectId() == null | oldTask.getProjectId().isEmpty()) return;
        @NotNull final String projectId = oldTask.getProjectId();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setId(taskId);
        task.setProjectId(projectId);
        map.put(task.getId(), task);
    }

    @Override
    public final void clearByProjectId(@NotNull final String projectId) {
        @NotNull final List<Task> allTasks = new ArrayList<>(map.values());
        for (@Nullable final Task task : allTasks) {
            if (task == null) continue;
            if (task.getProjectId().equals(projectId))
                map.remove(task);
        }
    }
}
