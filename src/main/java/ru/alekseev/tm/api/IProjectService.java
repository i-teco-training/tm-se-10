package ru.alekseev.tm.api;

import ru.alekseev.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    Project findOneByUserIdAndProjectId(String userId, String projectId);

    List<Project> findAllByUserId(String userId);

    void updateByUserIdProjectIdProjectName(String userId, String projectId, String name);

    void deleteByUserIdAndProjectId(String userId, String projectId);

    void clearByUserId(String userId);
}
