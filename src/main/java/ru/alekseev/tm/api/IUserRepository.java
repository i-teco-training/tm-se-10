package ru.alekseev.tm.api;

import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.enumerated.RoleType;

public interface IUserRepository extends IRepository<User> {

    User findOneByLoginAndPasswordHashcode(String login, String passwordHashcode);

    void deleteByLoginAndPassword(String login, String passwordHashcode);

    void addByLoginPasswordUserRole(String login, String passwordHashcode, RoleType roleType);
}
