package ru.alekseev.tm.api;

import ru.alekseev.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    Project findOneByUserIdAndProjectId(String userId, String projectId);

    List<Project> findAllByUserId(String userId);

    void deleteByUserId(String userId);

    void updateByUserIdProjectIdProjectName(String userId, String projectId, String projectName);

    void deleteByUserIdAndProjectId(String userId, String projectId);
}
