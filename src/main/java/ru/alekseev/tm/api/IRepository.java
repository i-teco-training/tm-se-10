package ru.alekseev.tm.api;

import ru.alekseev.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void addAll(List<E> list);

    E findOne(String id);

    List<E> findAll();

    void persist(E entity);

    void merge(E entity);

    void delete(String id);

    void clear();
}