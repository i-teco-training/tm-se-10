package ru.alekseev.tm.api;

import ru.alekseev.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    void clearByProjectId(String projectId);

    List<Task> findAllByUserId(String userId);

    void deleteByUserIdAndTaskId(String userId, String taskId);

    void updateByNewData(String userId, String taskId, String name);
}
