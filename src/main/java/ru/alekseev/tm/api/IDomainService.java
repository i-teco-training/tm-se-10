package ru.alekseev.tm.api;

import ru.alekseev.tm.dto.Domain;

public interface IDomainService {

    Domain getDomain();

    void setDomain(Domain domain);
}
