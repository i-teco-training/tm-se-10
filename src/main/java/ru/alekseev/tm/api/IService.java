package ru.alekseev.tm.api;

import ru.alekseev.tm.entity.AbstractEntity;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    void add(E entity);

    List<E> findAll();

    void update(E entity);

    void delete(String id);

    void clear();
}
