package ru.alekseev.tm.api;

import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.enumerated.RoleType;

public interface IUserService extends IService<User> {
    User getCurrentUser();

    void setCurrentUser(User currentUser);

    boolean isAuthorized();

    User findOneByLoginAndPassword(String login, String passwordHashcode);

    void deleteByLoginAndPassword(String login, String passwordHashcode);

    void addByLoginPasswordUserRole(String login, String passwordHashcode, RoleType roleType);
}
