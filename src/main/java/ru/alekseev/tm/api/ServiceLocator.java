package ru.alekseev.tm.api;

import ru.alekseev.tm.command.system.AbstractCommand;

import java.util.List;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    //IService getService();

    ITerminalService getTerminalService();

    IDomainService getDomainService();

    List<AbstractCommand> getCommands();
}
