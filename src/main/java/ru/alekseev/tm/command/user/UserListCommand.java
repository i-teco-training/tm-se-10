package ru.alekseev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.User;

import java.util.List;

public final class UserListCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "show-users";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Show list of all users";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LIST OF ALL USERS]");
        @Nullable final List<User> allUsers = serviceLocator.getUserService().findAll();
        if (allUsers.size() == 0) {
            System.out.println("LIST OF PROJECTS IS EMPTY");
        }
        for (int i = 0; i < allUsers.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") login:" + allUsers.get(i).getLogin() + ", userId: " + allUsers.get(i).getId());
        }
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
