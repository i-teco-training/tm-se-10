package ru.alekseev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.util.HashUtil;

public final class UserLoginCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "login";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Login into Project Manager";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LOGGING IN]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = serviceLocator.getTerminalService().getFromConsole();
        System.out.println("ENTER PASSWORD");
        @NotNull final String password = serviceLocator.getTerminalService().getFromConsole();
        @NotNull final String passwordHashcode = HashUtil.getMd5(password);
        @Nullable final User requiredUser =
                serviceLocator.getUserService().findOneByLoginAndPassword(login, passwordHashcode);
        serviceLocator.getUserService().setCurrentUser(requiredUser);
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
