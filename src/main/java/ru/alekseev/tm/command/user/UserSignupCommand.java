package ru.alekseev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.util.HashUtil;

public final class UserSignupCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "signup";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Create new USER account";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[ADDING OF NEW USER ACCOUNT]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = serviceLocator.getTerminalService().getFromConsole();
        System.out.println("ENTER PASSWORD");
        @NotNull final String password = serviceLocator.getTerminalService().getFromConsole();
        @NotNull final String passwordHashcode = HashUtil.getMd5(password);
        @NotNull final User newUser = new User();
        newUser.setLogin(login);
        newUser.setPasswordHashcode(passwordHashcode);
        serviceLocator.getUserService().add(newUser);
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
