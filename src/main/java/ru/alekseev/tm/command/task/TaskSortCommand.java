package ru.alekseev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.entity.User;

import java.util.Comparator;
import java.util.List;

public final class TaskSortCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "sort-tasks";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Show sorted list of all tasks";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SORTED LIST OF ALL TASKS]");
        @NotNull final User currentUser = serviceLocator.getUserService().getCurrentUser();
        @Nullable final List<Task> listForSorting = serviceLocator.getTaskService().findAllByUserId(currentUser.getId());
        if (listForSorting.size() == 0) {
            System.out.println("LIST OF TASKS IS EMPTY");
            return;
        }
        System.out.println("sort by?");
        System.out.println("1 - by date of creation, 2 - by start, 3 - by finish, 4 - by status");
        System.out.println("ENTER 1, 2, 3 or 4");
        @NotNull final String input = serviceLocator.getTerminalService().getFromConsole();
        switch (input) {
            case "1": listForSorting.sort(Comparator.comparing(Task::getCreatedOn)); break;
            case "2": listForSorting.sort(Comparator.comparing(Task::getDateStart)); break;
            case "3": listForSorting.sort(Comparator.comparing(Task::getDateFinish)); break;
            case "4": listForSorting.sort(Comparator.comparing(Task::getStatus)); break;
            default:
                System.out.println("Incorrect input. Try again.");
                return;
        }

        for (int i = 0; i < listForSorting.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") name:" + listForSorting.get(i).getName()
                    + ", taskId: " + listForSorting.get(i).getId()
                    + ", date of creation: " + listForSorting.get(i).getCreatedOn()
                    + ", start date: " + listForSorting.get(i).getDateStart()
                    + ", finish date: " + listForSorting.get(i).getDateFinish()
                    + ", status: " + listForSorting.get(i).getStatus());
        }
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
