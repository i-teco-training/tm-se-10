package ru.alekseev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.command.system.AbstractCommand;

public final class TaskDeleteCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "delete-task";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Delete task by id";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[DELETING OF TASK]");
        System.out.println("ENTER TASK ID");
        @NotNull final String taskId = serviceLocator.getTerminalService().getFromConsole();
        @NotNull final String currentUserId = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getTaskService().deleteByUserIdAndTaskId(currentUserId, taskId);
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
