package ru.alekseev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.command.system.AbstractCommand;

public final class TaskUpdateCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "update-task";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Update task";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[UPDATING OF TASK]");
        System.out.println("ENTER TASK ID");
        @NotNull final String taskId = serviceLocator.getTerminalService().getFromConsole();
        System.out.println("ENTER NEW NAME");
        @NotNull final String newName = serviceLocator.getTerminalService().getFromConsole();
        @NotNull final String currentUserId = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getTaskService().updateByNewData(currentUserId, taskId, newName);
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
