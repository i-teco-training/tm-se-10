package ru.alekseev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class TaskSearchCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "search-tasks";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Search for tasks";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SEARCH FOR TASKS]");
        @NotNull final User currentUser = serviceLocator.getUserService().getCurrentUser();
        @Nullable final List<Task> listForSearching =
                serviceLocator.getTaskService().findAllByUserId(currentUser.getId());
        if (listForSearching.size() == 0) {
            System.out.println("LIST OF TASKS IS EMPTY");
            return;
        }
        System.out.println("ENTER SEARCH REQUEST");
        @NotNull String input = serviceLocator.getTerminalService().getFromConsole();
        if (input.isEmpty()) {
            System.out.println("Incorrect input.");
            return;
        }
        @NotNull List<Task> resultList = new ArrayList<>();
        for (@Nullable final Task task : listForSearching) {
            if (task == null) continue;
            @Nullable String name = task.getName();
            @Nullable String description = task.getDescription();
            if ((name != null && name.contains(input)) || (description != null && description.contains(input)))
                resultList.add(task);
        }
        if (resultList.size() == 0) {
            System.out.println("NO RESULTS");
            return;
        }
        for (int i = 0; i < resultList.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") name:" + resultList.get(i).getName()
                    + ", taskId: " + resultList.get(i).getId()
                    + ", description: " + resultList.get(i).getDescription());
        }
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}