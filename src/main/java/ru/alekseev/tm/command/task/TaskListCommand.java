package ru.alekseev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.Task;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "show-tasks";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Show all tasks";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LIST OF ALL TASKS]");

        //@NotNull final User currentUser = serviceLocator.getUserService().getCurrentUser();
        //@Nullable final List<Task> list = serviceLocator.getTaskService().findAllByUserId(currentUser.getId());

        List<Task> list = serviceLocator.getTaskService().findAll();//временно доступ без авторизации

        if (list.size() == 0) {
            System.out.println("LIST OF TASKS IS EMPTY");
        }
        for (int i = 0; i < list.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") name:" + list.get(i).getName() + ", taskId: " + list.get(i).getId());
        }

    }

    @Override
    public final boolean isSecure() {
        return false;
    }//временно доступ без авторизации
}
