package ru.alekseev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.command.system.AbstractCommand;

public final class ProjectUpdateCommand extends AbstractCommand {
    @Override
    @NotNull public final String getName() {
        return "update-project";
    }

    @Override
    @NotNull public final String getDescription() {
        return "Update project";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[UPDATING OF PROJECT]");
        System.out.println("ENTER PROJECT ID");
        @NotNull String projectId = serviceLocator.getTerminalService().getFromConsole();
        System.out.println("ENTER NEW NAME");
        @NotNull String newName = serviceLocator.getTerminalService().getFromConsole();
        @NotNull String currentUserId = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getProjectService().updateByUserIdProjectIdProjectName(currentUserId, projectId, newName);
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
