package ru.alekseev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.Project;

public final class ProjectAddCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "add-project";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Add new project";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[ADDING OF NEW PROJECT]");
        System.out.println("ENTER NAME");
        @NotNull final String projectName = serviceLocator.getTerminalService().getFromConsole();
        if (projectName.isEmpty()) {
            System.out.println("invalid input!");
            return;
        }
        @NotNull final String currentUserId = serviceLocator.getUserService().getCurrentUser().getId();
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setUserId(currentUserId);
        serviceLocator.getProjectService().add(project);
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
