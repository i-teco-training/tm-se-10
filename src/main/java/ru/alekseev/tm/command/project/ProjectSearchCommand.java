package ru.alekseev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class ProjectSearchCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "search-projects";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Search for projects";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SEARCH FOR PROJECTS]");
        @NotNull final User currentUser = serviceLocator.getUserService().getCurrentUser();
        @Nullable final List<Project> listForSearching =
                serviceLocator.getProjectService().findAllByUserId(currentUser.getId());
        if (listForSearching.size() == 0) {
            System.out.println("LIST OF PROJECTS IS EMPTY");
            return;
        }
        System.out.println("ENTER SEARCH REQUEST");
        @NotNull String input = serviceLocator.getTerminalService().getFromConsole();
        if (input.isEmpty()) {
                System.out.println("Incorrect input.");
                return;
        }
        @NotNull List<Project> resultList = new ArrayList<>();
        for (@Nullable final Project project : listForSearching) {
            if (project == null) continue;
            @Nullable String name = project.getName();
            @Nullable String description = project.getDescription();
            if ((name != null && name.contains(input)) || (description != null && description.contains(input)))
                resultList.add(project);
        }
        if (resultList.size() == 0) {
            System.out.println("NO RESULTS");
            return;
        }
        for (int i = 0; i < resultList.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") name:" + resultList.get(i).getName()
                    + ", projectId: " + resultList.get(i).getId()
                    + ", description: " + resultList.get(i).getDescription());
        }
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}