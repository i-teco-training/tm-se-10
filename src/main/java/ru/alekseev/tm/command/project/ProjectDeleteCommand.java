package ru.alekseev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.command.system.AbstractCommand;

public final class ProjectDeleteCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "delete-project";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Delete project by id";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[DELETING OF PROJECT]");
        System.out.println("ENTER PROJECT ID");
        @NotNull final String projectId = serviceLocator.getTerminalService().getFromConsole();
        @NotNull final String currentUserId = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getProjectService().deleteByUserIdAndProjectId(currentUserId, projectId);
        serviceLocator.getTaskService().clearByProjectId(projectId);
        System.out.println("[OK. PROJECT AND IT'S TASKS DELETED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
