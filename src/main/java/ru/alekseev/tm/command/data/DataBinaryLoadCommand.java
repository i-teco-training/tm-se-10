package ru.alekseev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.dto.Domain;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public final class DataBinaryLoadCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "bin-load";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Binary load";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[BINARY LOAD]");
        System.out.println("ENTER PATH TO FILE");
        @NotNull final String filePath = serviceLocator.getTerminalService().getFromConsole();
        if (filePath.isEmpty()) return;

        @NotNull final FileInputStream fileInputStream = new FileInputStream(filePath);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @Nullable final Domain loadedDomain = (Domain) objectInputStream.readObject();
        fileInputStream.close();
        objectInputStream.close();
        if (loadedDomain == null) return;
        serviceLocator.getDomainService().setDomain(loadedDomain);
        System.out.println("[DATA LOADED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }

}
