package ru.alekseev.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.dto.Domain;

import java.io.File;
import java.io.FileWriter;

public final class DataXmlFasterxmlSaveCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "xml2-save";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "save to FasterXML XML";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SAVE PROJECT TO FASTERXML XML]");
        System.out.println("ENTER SAVE PATH");
        @NotNull final String filePath = serviceLocator.getTerminalService().getFromConsole();
        if (filePath.isEmpty()) return;
        @Nullable final Domain domainToXml = serviceLocator.getDomainService().getDomain();
        if (domainToXml == null) return;

        @NotNull final ObjectMapper xmlMapper = new XmlMapper();
        @NotNull final String xmlString = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domainToXml);
        @NotNull final File xmlOutput = new File(filePath);
        @NotNull final FileWriter fileWriter = new FileWriter(xmlOutput);
        fileWriter.write(xmlString);
        fileWriter.close();

        System.out.println("[DATA SAVED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}