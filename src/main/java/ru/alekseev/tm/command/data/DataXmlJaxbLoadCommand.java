package ru.alekseev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public final class DataXmlJaxbLoadCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "xml1-load";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "load from JAXB XML";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LOAD FROM JAXB XML]");
        System.out.println("ENTER PATH TO FILE");
        @NotNull final String filePath = serviceLocator.getTerminalService().getFromConsole();
        if (filePath.isEmpty()) return;

        @NotNull final File xmlFile = new File(filePath);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final Domain loadedDomain = (Domain) unmarshaller.unmarshal(xmlFile);
        if (loadedDomain == null) return;
        serviceLocator.getDomainService().setDomain(loadedDomain);
        System.out.println("[DATA LOADED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
