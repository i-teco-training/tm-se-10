package ru.alekseev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public final class DataJsonJaxbSaveCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "json1-save";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "save to JAXB JSON";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SAVE TO JAXB JSON]");
        System.out.println("ENTER SAVE PATH");
        @NotNull final String filePath = serviceLocator.getTerminalService().getFromConsole();
        if (filePath.isEmpty()) return;
        @Nullable final Domain domainToJson = serviceLocator.getDomainService().getDomain();
        if (domainToJson == null) return;

        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final File file = new File(filePath);
        marshaller.marshal(domainToJson, file);

        System.out.println("[DATA SAVED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}