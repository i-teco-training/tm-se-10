package ru.alekseev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public final class DataJsonJaxbLoadCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "json1-load";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "load from JAXB JSON";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LOAD FROM JAXB JSON]");
        System.out.println("ENTER PATH TO FILE");
        @NotNull final String filePath = serviceLocator.getTerminalService().getFromConsole();
        if (filePath.isEmpty()) return;

        @NotNull final File jsonFile = new File(filePath);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        @Nullable final Domain loadedDomain = (Domain) unmarshaller.unmarshal(jsonFile);
        if (loadedDomain == null) return;
        serviceLocator.getDomainService().setDomain(loadedDomain);
        System.out.println("[DATA LOADED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
