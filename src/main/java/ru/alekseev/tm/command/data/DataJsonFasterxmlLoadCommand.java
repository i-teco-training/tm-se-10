package ru.alekseev.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.dto.Domain;

import java.io.File;

public final class DataJsonFasterxmlLoadCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "json2-load";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "load from FasterXML JSON";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LOAD FROM FASTERXML JSON]");
        System.out.println("ENTER PATH TO FILE");
        @NotNull final String filePath = serviceLocator.getTerminalService().getFromConsole();
        if (filePath.isEmpty()) return;

        @Nullable final File file = new File(filePath);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final Domain loadedDomain = objectMapper.readValue(file, Domain.class);
        if (loadedDomain == null) return;
        serviceLocator.getDomainService().setDomain(loadedDomain);
        System.out.println("[DATA LOADED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}