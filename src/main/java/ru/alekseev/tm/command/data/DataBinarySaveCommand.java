package ru.alekseev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.dto.Domain;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public final class DataBinarySaveCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "bin-save";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Binary save";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[BINARY SAVE]");
        System.out.println("ENTER SAVE PATH");
        @NotNull final String filePath = serviceLocator.getTerminalService().getFromConsole();
        if (filePath.isEmpty()) return;
        @Nullable final Domain domainForSerialization = serviceLocator.getDomainService().getDomain();
        if (domainForSerialization == null) return;

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(filePath);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domainForSerialization);
        fileOutputStream.close();
        objectOutputStream.close();

        System.out.println("[DATA SAVED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}