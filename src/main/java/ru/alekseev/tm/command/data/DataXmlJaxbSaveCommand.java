package ru.alekseev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public final class DataXmlJaxbSaveCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "xml1-save";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "save to JAXB XML";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SAVE TO JAXB XML]");
        System.out.println("ENTER SAVE PATH");
        @NotNull final String filePath = serviceLocator.getTerminalService().getFromConsole();
        if (filePath.isEmpty()) return;
        @Nullable final Domain domainToXml = serviceLocator.getDomainService().getDomain();
        if (domainToXml == null) return;

        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final File file = new File(filePath);
        marshaller.marshal(domainToXml, file);
        System.out.println("[DATA SAVED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}