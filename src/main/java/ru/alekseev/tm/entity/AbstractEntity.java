package ru.alekseev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractEntity implements Serializable {

    private static final long serialversionUID = 1L;
    @NotNull private final String id = UUID.randomUUID().toString();
}
