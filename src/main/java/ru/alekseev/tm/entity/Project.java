package ru.alekseev.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.enumerated.Status;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
public final class Project extends AbstractEntity {
    @NotNull private String id = UUID.randomUUID().toString();
    @Nullable private String name;
    @Nullable private String description;
    @Nullable private Date dateStart;
    @Nullable private Date dateFinish;
    @Nullable private String userId;
    @NotNull private Status status = Status.PLANNED;
    @NotNull private Date createdOn = new Date();
}
