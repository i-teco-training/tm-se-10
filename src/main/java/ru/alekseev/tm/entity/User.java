package ru.alekseev.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.Setter;
import ru.alekseev.tm.enumerated.RoleType;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
public final class User extends AbstractEntity{
    @NotNull private final String id = UUID.randomUUID().toString();
    @Nullable private String login;
    @Nullable private String passwordHashcode;
    @Nullable private RoleType roleType;
}
